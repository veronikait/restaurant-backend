# Resturant Backend

![alt text](https://gitlab.com/veronikait/restaurant-backend/wikis/uploads/2a6b632791e5d710b698a4be0d84b81e/Screenshot_from_2019-02-10_22-38-36.png)



##  Introduction 

This artifacts represents the backend software of a restaurant. This is the core logic for search best strategies for assigning tables to clients in a virtual Restaurant. The engine software, also, expose a REST layer services for integrated with others GUI or any kind of system.   

##  Requirements
 
 The software requirements were to make a solution for a restaurant with the target of assigning in the best smart way the available tables to the arrivals clients.
 *The rules*
 *  There are N tables in the restaurants.
 *  Each table has minimum of two chairs and a maximum of six chairs.
 *  At the restaurant could arrive single clients or in group. 1 to 6.
 *  When a client arrive, the best option is assign a free table.
 *  If there is not a free table available, the client could join to share table if there are enough chairs.
 *  If there are not free or shared tables availables, the client must wait in the waiting list.
 *  In any moment, a client might leave a table allowing other clients to take the place.
 *  In any moment, a client might leave the waiting list.
 *  Always, the proirity is for the first client that has arrived to restaurant except if there are not tables and the other clients in the queue can fit it better.
 

##  Features
  *  The N tables are configurable.
  *  The range of chairs for table is configurable.
  *  The range of clients is configurable. 
  *  The console shows a simple table graphics.


##  Architecture

     For this solution, I chosed for a Micro-service architecuture with a monolitic backend based on Spring Boot and running on Undertow web server.
     The core solution expose a layer of REST services API for integration and uses purposes. 
     This solution was mounted in my own java project template, so there are a lot of files that not be concerning this project.

##  Configuration
     All configurable parameters are concentred in externalized file application-prod.yml. There are differents version of this file for each environment.
     
     The software is intenacionalizated following the i18N Standard. Implementations in English and spanish.

##  Technologies

   *  JAVA 11 (OpenJDK JVM - Apache Netbeans 10.0)
   *  Spring Boot 2.1.1 Vesion Framework
   *  Gradle 5.0
   *  Swagger Springbox 2.7.0 REST services documentation.

##  Test
   * For artifact test, there are some kind of cases. The most relevant is RestaurantRestTest developed for test directly the REST services and make assertions for return http state codes. 
    
    Each test case could be profiled incrementing the *benchmarkRounds* parameter for reach a software stress.

##  Run instructions
   * The simplest way to run the solution is running with **docker**.
   * just execute 
     
           docker push yackalahar/veronika:resto
  

           Then open a browser and go to the link that appear at the last line of the trace.


##  Usage modes
    1.  The first and easy approach is interact by the Swagger UI interface.
    2.  The second is install the Restless Chrome extension and import the restletConsume.json in /resources/main/rest/retletapi/client
    3.  The third way is execute the test case ar.com.application.restaurant.RestaurantSimulationTest.java. The test case migth allow configure the quantity of clients and tables for a complete simulation.
     
    

     

##  Devops
   * Gitlab CI pipelines
   * Docker distribution
   * The project has many sh executable script files for run create the docker with the last code, run it the container and other for push at the docker hub registry.
  

## Contributing

Please see the [contribution guidelines](CONTRIBUTING.md)


##  Author
Rafael Benedettelli - Veronika IT

rblbene@gmail.com

veronika.com.ar

gitlab.com/veronikait




