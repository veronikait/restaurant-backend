# Change Log
All notable changes to this project will be documented in this file.

## [Unreleased]
### Added
- Application run in docker

### Changed

## 2019-02-06
### Added
- Change Loggers
- Add contribution file
