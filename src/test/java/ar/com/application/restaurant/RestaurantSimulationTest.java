/*
 * Copyright 2019 Veronika.com.ar.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ar.com.application.restaurant;

import ar.com.application.exceptions.IllegalConfigurationException;
import ar.com.application.restaurant.domain.Client;
import ar.com.application.restaurant.domain.ClientGroup;
import ar.com.application.restaurant.domain.Table;
import ar.com.application.restaurant.helpers.RestaurantBuilderHelper;
import ar.com.application.restaurant.services.impl.RestManagerServiceImpl;
import com.carrotsearch.junitbenchmarks.AbstractBenchmark;
import com.carrotsearch.junitbenchmarks.BenchmarkOptions;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.List;
import java.util.Optional;
import java.util.Random;
import java.util.stream.Collectors;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.Before;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * This class is for testing proposes.
 * <p>
 * This class is test for class UserDao. This class has logic for make
 * benchmarks profiles.
 *
 * @author Rafael Benedettelli
 */
@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles(profiles = "test")
public class RestaurantSimulationTest extends AbstractBenchmark {

    @Autowired
    private RestManagerServiceImpl rest;

    private final static Random RANDOM = new Random();

    public static final int TOTAL_CLIENT_ARRIVE = 10;

    public static final int TOTAL_CLIENT_FOR_START_LEAVE = 5;

    @Before
    public void setup() throws MalformedURLException, IllegalConfigurationException {

    }

    @BenchmarkOptions(callgc = false, benchmarkRounds = 1, warmupRounds = 0)
    @Test
    public void simulateTest() throws IOException {

        RestaurantBuilderHelper.totalTables = 2;

        RestaurantBuilderHelper.show();

        for (int i = 0; i < TOTAL_CLIENT_ARRIVE; i++) {

            if (i >= TOTAL_CLIENT_FOR_START_LEAVE) {

                Optional<Client> client = getRandomClientInTable(rest.getTables());

                client.ifPresent(rest::onLeave);

            } else {

                rest.onArrive(generateRandomClient());

            }

        }

    }

    private static Client generateRandomClient() {

        int clientNumber = RANDOM.nextInt(6) + 1;

        if (clientNumber == 1) {

            return new Client("");

        } else {

            return new ClientGroup("", clientNumber);
        }

    }

    private static Optional<Client> getRandomClientInTable(List<Table> tables) {

        Optional<Client> optionalClient = Optional.empty();

        List<Table> busyTables = tables.stream().filter((t) -> {

            return !t.isFree();

        }).collect(Collectors.toList());

        if (!busyTables.isEmpty()) {

            int randomTable = RANDOM.nextInt(busyTables.size());

            Table busyTable = busyTables.get(randomTable);

            optionalClient = busyTable.getClients().stream().findFirst();

        }

        return optionalClient;
    }

}
