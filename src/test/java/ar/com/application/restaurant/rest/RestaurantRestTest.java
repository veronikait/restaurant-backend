/*
 * Copyright 2019 Veronika.com.ar.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ar.com.application.restaurant.rest;

import ar.com.application.ApplicationService;
import ar.com.application.ServerSource;
import ar.com.application.exceptions.IllegalConfigurationException;
import com.carrotsearch.junitbenchmarks.AbstractBenchmark;
import com.carrotsearch.junitbenchmarks.BenchmarkOptions;
import java.io.IOException;
import java.net.MalformedURLException;
import ar.com.application.restaurant.rest.domain.SimpleResult;
import static io.restassured.RestAssured.given;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.filter.log.RequestLoggingFilter;
import io.restassured.filter.log.ResponseLoggingFilter;
import io.restassured.http.ContentType;
import io.restassured.specification.RequestSpecification;
import io.undertow.util.StatusCodes;
import java.util.concurrent.CompletableFuture;
import static org.junit.Assert.assertTrue;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.Before;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * This class is for testing proposes.
 * <p>
 * This class is test for class UserDao. This class has logic for make
 * benchmarks profiles.
 *
 * @author Rafael Benedettelli
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles(profiles = "test")
public class RestaurantRestTest extends AbstractBenchmark {

    @Autowired
    private ApplicationService applicationService;

    @LocalServerPort
    private int port;

    private RequestSpecification SPECIFICATION;

    private String apiRestaurant;

    @Before
    public void setup() throws MalformedURLException, IllegalConfigurationException {

        ServerSource server = applicationService.getServerInformation();

        String host = server.getHost();

        String contextPath = applicationService.getServletContextPath();

        contextPath = contextPath.replace("/", "");

        SPECIFICATION = new RequestSpecBuilder()
                .setContentType(ContentType.JSON)
                .setBaseUri("http://" + host + ":" + port + "/" + contextPath)
                .addFilter(new ResponseLoggingFilter())
                .addFilter(new RequestLoggingFilter())
                .build()
                .auth().basic(applicationService.getServerAuthenticationUsername(),
                        applicationService.getServerAuthenticationPassword());

        apiRestaurant = applicationService.getApplicationRestPathPrefix();
    }

    @BenchmarkOptions(callgc = false, benchmarkRounds = 1, warmupRounds = 0)
    @Test
    public void onNewClientArrive_AssignMostAccurateTable() throws IOException {

        SimpleResult result = simulateArriveClients(4);

        final int ID_GENERATED_CLIENT_LENGTH = 36;

        //Assertions.....................................................................
        assertTrue(!result.isError());
        assertTrue(result.getObject().toString().length() == ID_GENERATED_CLIENT_LENGTH);

    }

    @BenchmarkOptions(callgc = false, benchmarkRounds = 1, warmupRounds = 0)
    @Test
    public void onClientLeave_ClientLeaveTheTable() throws IOException {

        CompletableFuture.completedFuture(this.simulateArriveClients(2)).
                thenAccept((res) -> {

                    String clientID = (String) res.getObject();

                    String VERB = "client";

                    SimpleResult result = given()
                            .spec(SPECIFICATION)
                            .when()
                            .delete(apiRestaurant + "/" + VERB + "/" + clientID)
                            .then()
                            .statusCode(StatusCodes.ACCEPTED)
                            .extract()
                            .as(SimpleResult.class);

                    //Assertions.....................................................................
                    assertTrue(!result.isError());

                });

    }

    @BenchmarkOptions(callgc = false, benchmarkRounds = 1, warmupRounds = 0)
    @Test
    public void retriveWaitingList() throws IOException {

        CompletableFuture.completedFuture(this.simulateArriveClients(2)).
                thenAccept((t) -> {

                    String VERB = "waitingList";

                    SimpleResult result = given()
                            .spec(SPECIFICATION)
                            .when()
                            .get(apiRestaurant + "/" + VERB)
                            .then()
                            .statusCode(StatusCodes.OK)
                            .extract()
                            .as(SimpleResult.class);

                    //Assertions.....................................................................
                    assertTrue(!result.isError());

                });

    }

    @BenchmarkOptions(callgc = false, benchmarkRounds = 1, warmupRounds = 0)
    @Test
    public void retriveRestaurantStatus() throws IOException {

        CompletableFuture.completedFuture(this.simulateArriveClients(2)).
                thenAccept((t) -> {

                    String VERB = "status";

                    SimpleResult result = given()
                            .spec(SPECIFICATION)
                            .when()
                            .get(apiRestaurant + "/" + VERB)
                            .then()
                            .statusCode(StatusCodes.OK)
                            .extract()
                            .as(SimpleResult.class);

                    //Assertions.....................................................................
                    assertTrue(!result.isError());

                });

    }

    private SimpleResult simulateArriveClients(int size) {

        final String VERB = "client";

        return given()
                .spec(SPECIFICATION)
                .when()
                .post(apiRestaurant + "/" + VERB + "/" + size)
                .then()
                .statusCode(StatusCodes.CREATED)
                .extract()
                .as(SimpleResult.class);

    }

}
