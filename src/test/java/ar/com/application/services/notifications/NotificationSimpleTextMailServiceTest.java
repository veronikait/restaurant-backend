/*
 * Copyright 2019 Veronika.com.ar.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ar.com.application.services.notifications;

import ar.com.application.dao.*;

import ar.com.application.domain.User;
import ar.com.application.exceptions.DataAccessLayerException;
import ar.com.application.exceptions.IllegalConfigurationException;
import com.carrotsearch.junitbenchmarks.AbstractBenchmark;
import com.carrotsearch.junitbenchmarks.BenchmarkOptions;
import java.io.IOException;
import java.net.MalformedURLException;
import static ar.com.application.ApplicationService.LOGGER;
import java.time.LocalDate;
import static org.junit.Assert.assertTrue;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.Before;
import org.junit.experimental.categories.Category;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import uk.co.jemos.podam.api.PodamFactory;

/**
 * This class is for testing proposes.
 * <p>
 * This class is test for class UserDao. This class has logic for make
 * benchmarks profiles.
 *
 * @author Rafael Benedettelli
 */
@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles(profiles = "test")
public class NotificationSimpleTextMailServiceTest extends AbstractBenchmark {

    @Autowired
    private PodamFactory podamFactory;

    @Autowired
    private NotificationSimpleTextMailService notificationSimpleTextMailService;

    @Mock
    private NotificationSimpleTextMailService mockNotificationSimpleTextMailService;

    private User user;

    @Before
    public void setup() throws MalformedURLException, IllegalConfigurationException {

    }

    /*@BenchmarkOptions(callgc = false, benchmarkRounds = 1, warmupRounds = 0)
    @Test    
    public void buildSimpleMailService_sendDefaultProjectMail() throws IOException {

        user = podamFactory.manufacturePojo(User.class);
        user.setUsername("rblbene@gmail.com");
        user.setBirthDate(LocalDate.now());

        notificationSimpleTextMailService.addUser(user);
        notificationSimpleTextMailService.setContent("Prueba contenido de mail");

        boolean notificated = notificationSimpleTextMailService.notificate();

        LOGGER.info("Notification sended");

        //Assertions.....................................................................
        assertTrue(notificated);

    }*/

    @BenchmarkOptions(callgc = false, benchmarkRounds = 4, warmupRounds = 0)
    @Test
    @Category(BenchmarkMethod.class)
    public void testMock() throws DataAccessLayerException {

        Mockito.when(mockNotificationSimpleTextMailService.notificate()).thenReturn(true);

          var result = mockNotificationSimpleTextMailService.notificate();

        assertTrue(result);

    }
}
