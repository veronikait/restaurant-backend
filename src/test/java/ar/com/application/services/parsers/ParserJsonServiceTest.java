/*
 * Copyright 2019 Veronika.com.ar.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ar.com.application.services.parsers;

import ar.com.application.services.parsers.ParserJsonService;
import ar.com.application.dao.*;

import ar.com.application.domain.User;
import ar.com.application.exceptions.DataAccessLayerException;
import ar.com.application.exceptions.IllegalConfigurationException;
import ar.com.application.helpers.FileHelper;
import com.carrotsearch.junitbenchmarks.AbstractBenchmark;
import com.carrotsearch.junitbenchmarks.BenchmarkOptions;
import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.Path;
import java.nio.file.Paths;
import static ar.com.application.ApplicationService.LOGGER;
import ar.com.application.test.suites.TestHelper;
import java.io.File;
import java.time.LocalDate;
import org.junit.Assert;
import static org.junit.Assert.assertNotNull;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.Before;
import org.junit.experimental.categories.Category;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import uk.co.jemos.podam.api.PodamFactory;

/**
 * This class is for testing proposes.
 * <p>
 * This class is test for class UserDao. This class has logic for make
 * benchmarks profiles.
 *
 * @author Rafael Benedettelli
 */
@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles(profiles = "test")
public class ParserJsonServiceTest extends AbstractBenchmark {

    @Autowired
    private PodamFactory podamFactory;

    private ParserService realParserService;

    @Mock
    private ParserJsonService mockParserService;

    private User user; 
    
    private Path userFile;

    private static final String FILE_TEST_NAME = "userSerialized.json";

    @Before
    public void setup() throws MalformedURLException, IllegalConfigurationException {

       LOGGER.info("setup before test");                
               
       String jsonFileTestPath = TestHelper.getPathForTestFiles().toString()
                + File.separator
                + FILE_TEST_NAME;
        
        userFile = Paths.get(jsonFileTestPath);
        
        realParserService = ParserFactoryService.getParserService(userFile);
    }

    @BenchmarkOptions(callgc = false, benchmarkRounds = 1, warmupRounds = 0)
    @Test
    public void serializeJsonFromUser_ThenParseJsonFileToUser_ThenGetNewUser() throws IOException {

        user = podamFactory.manufacturePojo(User.class);
        user.setBirthDate(LocalDate.now());
        user.setGroup(null);
        

        realParserService.serialize(userFile, user);

        String contentFile = FileHelper.getStringFromFile(userFile.toAbsolutePath().toString());

        LOGGER.info("new file created at : " + userFile.toAbsolutePath().toString());
        LOGGER.debug(contentFile);

        User userFromJson = (User) realParserService.parse(userFile, User.class);

        LOGGER.info("User citizen " + userFromJson.getCitizen());
        LOGGER.info("json Date " + userFromJson.getBirthDate());
        LOGGER.info("id " + userFromJson.getId());
        LOGGER.info("username" + userFromJson.getUsername());
        LOGGER.info("- Photo " + userFromJson.getPhoto());
        LOGGER.info("- Place - country: " + userFromJson.getPlace().getCountry());

        //Assertions.....................................................................
        assertNotNull(userFromJson);
        Assert.assertEquals(userFromJson.getCitizen(), user.getCitizen());
        Assert.assertEquals(userFromJson.getBirthDate(), user.getBirthDate());
        Assert.assertEquals(userFromJson.getPlace().getCity(), user.getPlace().getCity());

    }

    @BenchmarkOptions(callgc = false, benchmarkRounds = 4, warmupRounds = 0)
    @Test
    @Category(BenchmarkMethod.class)
    public void testMock() throws DataAccessLayerException {

        /* var user = new User();

        Mockito.when(mockParserService.readJsonWithObjectMapper()).thenReturn(Optional.of(user));

          var userResult = mockParserService.get(3341L).get();

        assertEquals(userResult, user);*/
    }
}
