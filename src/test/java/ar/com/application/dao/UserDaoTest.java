/*
 * Copyright 2019 Veronika.com.ar.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ar.com.application.dao;

import ar.com.application.ApplicationService;
import static ar.com.application.ApplicationService.LOGGER;
import ar.com.application.domain.Group;

import ar.com.application.domain.User;
import ar.com.application.exceptions.DataAccessLayerException;
import ar.com.application.exceptions.IllegalConfigurationException;
import com.carrotsearch.junitbenchmarks.AbstractBenchmark;
import com.carrotsearch.junitbenchmarks.BenchmarkOptions;
import java.net.MalformedURLException;
import java.util.Optional;
import org.junit.Assert;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.Before;
import org.junit.experimental.categories.Category;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import uk.co.jemos.podam.api.PodamFactory;

/**
 * This class is for testing proposes.
 * <p>
 * This class is test for class UserDao. This class has logic for make
 * benchmarks profiles.
 *
 * @author Rafael Benedettelli
 */
@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles(profiles = "test")
public class UserDaoTest extends AbstractBenchmark {

    @Autowired
    private ApplicationService serviceInitilizeApp;

    @Autowired
    private PodamFactory podamFactory;

    @Autowired
    private UserDao realUserDao;

    @Mock
    private UserDao mockUserDao;

    @Autowired
    private GroupDao realGroupDao;

    private User user;

    private Group group;

    @Before
    public void setup() throws MalformedURLException, IllegalConfigurationException {

        LOGGER.info("setup before test");

        serviceInitilizeApp.init();

        user = podamFactory.manufacturePojo(User.class);

        //set null value of podam 
        user.getPlace().setId(null);
        user.setGroup(null);

        group = podamFactory.manufacturePojo(Group.class);
        group.setId(null);
        group.setUsers(null);

    }

    @BenchmarkOptions(callgc = false, benchmarkRounds = 1, warmupRounds = 0)
    @Test
    public void save_newUser_getNewUser() throws DataAccessLayerException {

        User userCreated = realUserDao.save(user);

        assertNotNull(userCreated.getId());
        assertNotNull(userCreated.getPlace().getId());

    }

    @BenchmarkOptions(callgc = false, benchmarkRounds = 1, warmupRounds = 0)
    @Test
    public void save_getUser_retriveExistUser() throws DataAccessLayerException {

        final Long ID = 1l;

        User userRetrived = realUserDao.get(ID).get();

        Assert.assertEquals(userRetrived.getId(), ID);
        assertNotNull(userRetrived.getPlace().getId());

        LOGGER.info("Citizen : " + userRetrived.getCitizen());
        LOGGER.info("City : " + userRetrived.getPlace().getCity());

    }

    @BenchmarkOptions(callgc = false, benchmarkRounds = 1, warmupRounds = 0)
    @Test
    public void saveGroup_putUser() throws DataAccessLayerException {

        final Long ID = 1l;

        User userRetrived = realUserDao.get(ID).get();

        group.setDescription("The new group");
        group.addUser(userRetrived);

        realGroupDao.save(group);

    }

    @BenchmarkOptions(callgc = false, benchmarkRounds = 4, warmupRounds = 0)
    @Test
    @Category(BenchmarkMethod.class)
    public void testMock() throws DataAccessLayerException {

          var user = new User();

        Mockito.when(mockUserDao.get(3341L)).thenReturn(Optional.of(user));

          var userResult = mockUserDao.get(3341L).get();

        assertEquals(userResult, user);

    }
}
