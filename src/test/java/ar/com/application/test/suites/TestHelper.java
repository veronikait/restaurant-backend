/*
 * Copyright 2019 Veronika.com.ar.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ar.com.application.test.suites;

import static ar.com.application.ApplicationService.LOGGER;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 *
 * @author Rafael Benedettelli
 */
public class TestHelper {

    private static final String PATH_TEST_FILES = "build/testFiles";

    public static Path getPathForTestFiles() {

        Path path = Paths.get(PATH_TEST_FILES);

        if (!Files.exists(path)) {

            try {

                Files.createDirectory(path);

            } catch (IOException ex) {

                LOGGER.info("Error at try to create temporal test folder " + ex);
            }
        }

        return path;

    }

}
