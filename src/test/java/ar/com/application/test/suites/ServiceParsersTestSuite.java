/*
 * Copyright 2019 Veronika.com.ar.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ar.com.application.test.suites;

import ar.com.application.services.parsers.ParserJsonServiceTest;
import ar.com.application.services.parsers.ParserXmlServiceTest;
import ar.com.application.services.parsers.ParserYamlServiceTest;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

/**
 * This class is for testing proposes.
 * <p>
 * This is a suite of test classes. This class is the entry-point for execute
 * all class defined in Suit.suiteClasses.
 *
 * @author Rafael Benedettelli
 */
//TODO incluir todos los testings en SuiteClasses
@RunWith(Suite.class)
@Suite.SuiteClasses({ParserXmlServiceTest.class,
                     ParserJsonServiceTest.class, 
                     ParserYamlServiceTest.class,
                     })
public class ServiceParsersTestSuite {

}
