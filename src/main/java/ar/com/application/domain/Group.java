/*
 * Copyright 2019 Veronika.com.ar.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ar.com.application.domain;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * The {@code Group} class represents a base DTO Data Transfer Object. This kind
 * of object is entity and uses for persistence.
 * <p>
 * This entity is the object group of users.
 *
 * @author Rafael Benedettelli
 * @see ar.com.application.domain#BaseEntity
 * @see ar.com.application.domain#User
 *
 * @since 1.0
 */
@Entity
@Table(name = "UsersGroup")
public class Group extends BaseEntity {

    private String groupName;

    //Mapped by es fundamental para que sea bidireccional y no cree tabla intermedia de relacion
    //Cascade all tambien para crear el join
    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, mappedBy = "group")
    private List<User> users;

    public Group(String groupName) {

        this.groupName = groupName;

    }

    public Group() {

    }

    public List<User> getUsers() {
        return users;
    }

    public void setUsers(List<User> users) {
        this.users = users;
    }

    public void addUser(User user) {

        if (users == null) {
            users = new ArrayList<>();
        }

        users.add(user);

        user.setGroup(this);

    }

    public void removeUser(User user) {

        users.remove(user);

        user.setGroup(null);

    }

}
