/*
 * Copyright 2019 Veronika.com.ar.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ar.com.application.domain;

/**
 * The {@code Range} class represents a base DTO Data Transfer Object.
 * <p>
 * This is a helper object for use in number attributes that may defined in
 * range interval of numbers.
 *
 * @author Rafael Benedettelli
 * @see ar.com.application.domain#BaseEntity
 *
 * @since 1.0
 */
public class Range {

    private int positionMax;

    private int positionMin;

    public int getPositionMax() {
        return positionMax;
    }

    public void setPositionMax(int positionMax) {
        this.positionMax = positionMax;
    }

    public int getPositionMin() {
        return positionMin;
    }

    public void setPositionMin(int positionMin) {
        this.positionMin = positionMin;
    }

}
