/*
 * Copyright 2019 Veronika.com.ar.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ar.com.application.domain;

import javax.persistence.Entity;

/**
 * The {@code Place} class represents a base DTO Data Transfer Object. This kind
 * of object is entity and uses for persistence.
 * <p>
 * This entity is the Place and is common get a relationship between all kind of
 * object that has a geography location such as person or organization.
 *
 * @author Rafael Benedettelli
 * @see ar.com.application.domain#BaseEntity
 * @see ar.com.application.domain#User
 *
 * @since 1.0
 */
@Entity
public class Place extends BaseEntity {

    private String continent;

    private String region;

    private String country;

    private String province;

    private String city;

    private String village;

    private String address;

    public Place() {

        this.city = city;

    }

    public Place(String city) {

        this.city = city;

    }

    public String getContinent() {
        return continent;
    }

    public void setContinent(String continent) {
        this.continent = continent;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getVillage() {
        return village;
    }

    public void setVillage(String village) {
        this.village = village;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

}
