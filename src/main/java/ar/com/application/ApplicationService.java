/*
 * Copyright 2019 Veronika.com.ar.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ar.com.application;

import ar.com.application.restaurant.domain.RestaurantConfiguration;
import ar.com.application.exceptions.IllegalConfigurationException;
import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.file.Paths;
import java.util.Collections;
import java.util.Locale;
import java.util.Properties;
import java.util.ResourceBundle;
import javax.persistence.Entity;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.cfg.Environment;
import org.reflections.Reflections;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Service;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * The {@code ApplicationService} class represents the Application Service
 * initial startup for application. This concrete class has all logic about the
 * elemental configuration required by the software.
 * <p>
 * This class is critical and initialize all important tasks configurations for
 * the project such i18n, databases connection, spring configurations, etc.
 *
 * @author Rafael Benedettelli
 * @see ar.com.application#ApplicationService()
 * @see ar.com.application#DatabaseSource
 * @see ar.com.application#DatabaseSourceActive
 * @since 1.0
 */
@org.springframework.context.annotation.Configuration
@Service
@EnableSwagger2
public class ApplicationService {

    @Value("${spring.application.name}")
    private String applicationName;

    @Value("${spring.application.restpath}")
    private String applicationRestPathPrefix;

    @Value("${spring.application.locale.lang}")
    private String applicationLocaleLang;
    //Getters and setters omitted for brevity

    @Value("${spring.application.locale.country}")
    private String applicationLocaleCountry;
    
    @Value("${server.servlet.contextPath}")
    private String servletContextPath;
    
    @Value("${server.authentication.username}")
    private String serverAuthenticationUsername;
    
    @Value("${server.authentication.password}")
    private String serverAuthenticationPassword;


    private SessionFactory sessionFactory;

    public static ResourceBundle I18N;

    public static Logger LOGGER;

    private DatabaseSource databaseSourceSelected;

    /**
     * The configuration directory..
     */
    public static final String CONFIGURATION_DIRECTORY = "conf";

    /**
     * The default path setting in environment variable.
     */
    public static final String APP_HOME = System.getenv("APPLICATION_HOME");

    @Autowired
    private ApplicationProperties configurationProperties;

    /**
     * Force fatal error exit status code.
     */
    public static final int STATUS_ERROR_EXIT = -1;

    public void init() throws MalformedURLException, IllegalConfigurationException {

        if (APP_HOME == null || APP_HOME.equals("")) {

            System.err.println(":::ENVIRONMENT PROBLEM:::::");

            System.err.println("Must set APPLICATION_HOME environment variable with absolute path of configuration files. ");

            System.err.println("Sample (APPLICATION_HOME=\"/opt/appHome\") in your SO before the deployment");

            System.exit(STATUS_ERROR_EXIT);

        }

        LOGGER = LogManager.getLogger(ApplicationService.class.getName());

        LOGGER.info("Start " + applicationName);

        LOGGER.info("Local: " + applicationLocaleLang + "-" + applicationLocaleCountry);

        initI18n();

        initDatabase();

    }

    /**
     * Method only focused in initialize I18n internationalization framework.
     * This method take internally the properties parameters for country and
     * languages indicated in config.properties file application
     */
    private void initI18n() throws MalformedURLException {

          var currentLocale = new Locale(applicationLocaleLang,
                applicationLocaleCountry);

        URL[] urls = {Paths.get(APP_HOME
            + File.separator
            + CONFIGURATION_DIRECTORY
            + File.separator
            + "i18n").
            toFile().
            toURI().
            toURL()};

        //ClassLoader loader = new URLClassLoader(urls);
        //TODO ojo cambiar como toma los resources bundles cuando genere el jar. 
        //En ese caso va a tener que usar el codigo de arriba
        I18N = ResourceBundle.getBundle("config.i18n.MessagesBundle", currentLocale);

    }

    private void initDatabase() throws IllegalConfigurationException {

        this.databaseSourceSelected = configurationProperties.
                getDatabases().
                stream().
                findAny().
                filter((d) -> {
                    return d.getActive().equalsIgnoreCase(DatabaseSourceActive.DEFAULT.toString());
                }).orElseThrow(() -> new IllegalConfigurationException("No database selected in configuration file"));

        if (sessionFactory == null) {
            try {
                  var configuration = new Configuration();
                // Hibernate settings equivalent to hibernate.cfg.xml's properties

                  var settings = new Properties();
                settings.put(Environment.DRIVER, databaseSourceSelected.getDriver());
                settings.put(Environment.URL, databaseSourceSelected.getUrl());
                settings.put(Environment.USER, databaseSourceSelected.getUser());
                settings.put(Environment.PASS, databaseSourceSelected.getPass());
                settings.put(Environment.DIALECT, databaseSourceSelected.getDialect());
                settings.put(Environment.SHOW_SQL, databaseSourceSelected.isShowSql());
                settings.put(Environment.CURRENT_SESSION_CONTEXT_CLASS, "thread");
                settings.put(Environment.HBM2DDL_AUTO, databaseSourceSelected.getDdlAuto());

                //spring.jpa.hibernate.ddl-auto=update
                configuration.setProperties(settings);

                /* configuration.addAnnotatedClass(Group.class);                
                configuration.addAnnotatedClass(User.class);                
                configuration.addAnnotatedClass(Persona.class);
                configuration.addAnnotatedClass(Lugar.class);*/
                addAnnotatedAllClasesOverThePackage(configuration, "ar.com.application.domain");

                  var serviceRegistry = new StandardServiceRegistryBuilder()
                        .applySettings(configuration.getProperties()).build();

                sessionFactory = configuration.buildSessionFactory(serviceRegistry);

            } catch (Exception e) {

                LOGGER.error("Error at try to make connection " + e);
            }
        }

    }

    public void addAnnotatedAllClasesOverThePackage(Configuration configuration, String packagePath) {

          var reflections = new Reflections(packagePath);

        reflections.getTypesAnnotatedWith(Entity.class).stream().forEach((t) -> {

            try {

                configuration.addAnnotatedClass(Class.forName(t.getTypeName()));

            } catch (ClassNotFoundException e) {

                LOGGER.error("Error at try to add annotated class hibernate: " + e);

            }

        });

    }

    public SessionFactory getSessionFactory() {

        return sessionFactory;
    }

    public DatabaseSource getDatabaseSourceSelected() {
        return databaseSourceSelected;
    }

    public String getApplicationName() {
        return applicationName;
    }

    public String getApplicationRestPathPrefix() {
        return applicationRestPathPrefix;
    }

    public ServerSource getServerInformation() {

        return this.configurationProperties.getServer();
    }

    public NotificationSource getNotifiactionSource() {

        return this.configurationProperties.getNotification();
    }

    public RestaurantConfiguration getRestaurantConfiguration() {

        return this.configurationProperties.getRestaurant();
    }

    public String getApplicationLocaleLang() {
        return applicationLocaleLang;
    }

    public String getApllicationLocaleCountry() {
        return applicationLocaleCountry;
    }

    public static Logger getLOGGER() {
        return LOGGER;
    }

    @Bean
    public Docket api() throws MalformedURLException {

        if (I18N == null) {
            initI18n();
        }

        return new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(apiSwaggerInfo())
                .select()
                .apis(RequestHandlerSelectors.basePackage("ar.com.application.restaurant.rest"))
                .paths(PathSelectors.any())
                .build();
    }

    private ApiInfo apiSwaggerInfo() {

        return new ApiInfo(
                I18N.getString("app.name"),
                I18N.getString("app.description"),
                I18N.getString("app.version"),
                I18N.getString("app.licence.description"),
                new Contact(
                        I18N.getString("app.author"),
                        I18N.getString("app.author.url"),
                        I18N.getString("app.author.mail")),
                I18N.getString("app.licence.title"),
                I18N.getString("app.licence.url"),
                Collections.emptyList());

    }

    public String getServletContextPath() {
        return servletContextPath;
    }

    public void setServletContextPath(String servletContextPath) {
        this.servletContextPath = servletContextPath;
    }

    public String getServerAuthenticationUsername() {
        return serverAuthenticationUsername;
    }

    public void setServerAuthenticationUsername(String serverAuthenticationUsername) {
        this.serverAuthenticationUsername = serverAuthenticationUsername;
    }

    public String getServerAuthenticationPassword() {
        return serverAuthenticationPassword;
    }

    public void setServerAuthenticationPassword(String serverAuthenticationPassword) {
        this.serverAuthenticationPassword = serverAuthenticationPassword;
    }
    
     

}
