/*
 * Copyright 2019 Veronika.com.ar.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ar.com.application;

/**
 * The {@code DatabaseSource} class represents a storage data persistence.
 * typically {@code DatabaseSource} is a common database such mySql or a noSql
 * database such MongoDB.
 * <p>
 * This class is prototyped as DTO (Data Transfer Object) This domain class
 * transport information of database such Name, driver, URL, user, pass, dialect
 * and all properties required for make connection and use the storage for save
 * and retrieve data.
 *
 * @author Rafael Benedettelli
 * @see ar.com.application#ApplicationService
 * @see ar.com.application#DatabaseSource
 * @see ar.com.application#DatabaseSourceActive
 * @since 1.0
 */
public class DatabaseSource {

    private String name;

    private String driver;

    private String url;

    private String user;

    private String pass;

    private String dialect;

    private boolean showSql;

    private String active;

    private String ddlAuto;

    public String getDriver() {
        return driver;
    }

    public void setDriver(String driver) {
        this.driver = driver;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getPass() {
        return pass;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDialect() {
        return dialect;
    }

    public void setDialect(String dialect) {
        this.dialect = dialect;
    }

    public boolean isShowSql() {
        return showSql;
    }

    public void setShowSql(boolean showSql) {
        this.showSql = showSql;
    }

    public String getActive() {
        return active;
    }

    public void setActive(String active) {
        this.active = active;
    }

    public String getDdlAuto() {
        return ddlAuto;
    }

    public void setDdlAuto(String ddlAuto) {
        this.ddlAuto = ddlAuto;
    }

}
