/*
 * Copyright 2019 Veronika.com.ar.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ar.com.application;

import ar.com.application.restaurant.domain.RestaurantConfiguration;
import java.util.ArrayList;
import java.util.List;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * The {@code ApplicationProperties} class represents the properties of
 * configuration. All application properties, such as {@code "databases"}, are
 * handled by this class.
 * <p>
 * This class contains databases and noSql databases list that client get for
 * know all persistence storage properties allowed for this app.
 *
 *
 * @author Rafael Benedettelli
 * @see ar.com.application#ApplicationService()
 * @see ar.com.application#DatabaseSource
 * @see ar.com.application#DatabaseSourceActive
 * @since 1.0
 */
@Configuration
@EnableConfigurationProperties
@ConfigurationProperties
public class ApplicationProperties {

    /**
     * Notification SMTP source configured in application external file.
     */
    private NotificationSource notification = new NotificationSource();

    /**
     * All databases configured in application external file.
     */
    private List<DatabaseSource> databases = new ArrayList<>();

    /**
     * All databases noSql configured in application external file.
     */
    private List<DatabaseSource> nosqldatabases = new ArrayList<>();

    /**
     * Server source with complete environment information.
     */
    private ServerSource server = new ServerSource();

    /**
     * Restaurant initial parameters.
     */
    private RestaurantConfiguration restaurant = new RestaurantConfiguration();

    public List<DatabaseSource> getDatabases() {

        return databases;
    }

    public void setDatabases(List<DatabaseSource> databases) {
        this.databases = databases;
    }

    public List<DatabaseSource> getNosqldatabases() {
        return nosqldatabases;
    }

    public void setNosqldatabases(List<DatabaseSource> nosqldatabases) {
        this.nosqldatabases = nosqldatabases;
    }

    public NotificationSource getNotification() {
        return notification;
    }

    public void setNotification(NotificationSource notification) {
        this.notification = notification;
    }

    public RestaurantConfiguration getRestaurant() {
        return restaurant;
    }

    public void setRestaurant(RestaurantConfiguration restaurant) {
        this.restaurant = restaurant;
    }

    public ServerSource getServer() {
        return server;
    }

    public void setServer(ServerSource server) {
        this.server = server;
    }

}
