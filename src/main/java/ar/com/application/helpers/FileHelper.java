/*
 * Copyright 2019 Veronika.com.ar.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ar.com.application.helpers;

import static ar.com.application.ApplicationService.LOGGER;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.stream.Stream;
import org.apache.commons.io.FilenameUtils;

/**
 *
 * @author Rafael Benedettelli
 */
public class FileHelper {

    public static String getStringFromFile(Path path) {

        return getStringFromFile(path.toAbsolutePath().toString());

    }

    public static String getStringFromFile(File file) {

        return getStringFromFile(file.getAbsoluteFile().toString());

    }

    public static String getStringFromFile(String fileName) {

        StringBuilder sb = new StringBuilder();

        //read file into stream, try-with-resources
        try (Stream<String> stream = Files.lines(Paths.get(fileName))) {

            stream.forEach(sb::append);

        } catch (IOException e) {

            LOGGER.error("Error at try to read String from file " + e);
        }

        return sb.toString();
    }
    
    public static String getFileExtension(Path path){
        
        return FilenameUtils.getExtension(path.toAbsolutePath().toString());
    }
}
