/*
 * Copyright 2019 Veronika.com.ar.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ar.com.application.helpers;

import java.io.IOException;

/**
 *
 * @author Rafael Benedettelli
 */
public class CommandLineHelper {

    public void openUrl(String url) throws IOException {

        String os = System.getProperty("os.name").toLowerCase();
               
        if (os.startsWith("win")){
            
            openWindows(url);
            
        }else  if (os.startsWith("mac")){
            
            openMac(url);
            
        }else  if (os.startsWith("nix") || os.toLowerCase().startsWith("linux")){
            
            openLinux(url);
            
        }

    }

    private void openWindows(String url) throws IOException {

        Runtime rt = Runtime.getRuntime();
        rt.exec("rundll32 url.dll,FileProtocolHandler " + url);

    }

    private void openMac(String url) throws IOException {

        Runtime rt = Runtime.getRuntime();
        rt.exec("open " + url);

    }

    private void openLinux(String url) throws IOException {

        Runtime rt = Runtime.getRuntime();

        String[] browsers = {"chrome","epiphany", "firefox", "mozilla", "konqueror",
            "netscape", "opera", "links", "lynx"};

        StringBuilder cmd = new StringBuilder();

        for (int i = 0; i < browsers.length; i++) {
            if (i == 0) {
                cmd.append(String.format("%s \"%s\"", browsers[i], url));
            } else {
                cmd.append(String.format(" || %s \"%s\"", browsers[i], url));
            }
        }
        // If the first didn't work, try the next browser and so on

        rt.exec(new String[]{"sh", "-c", cmd.toString()});
    }

}
