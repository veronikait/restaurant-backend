/*
 * Copyright 2019 Veronika.com.ar.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ar.com.application.helpers;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.Locale;
import org.springframework.stereotype.Service;

/**
 *
 * @author Rafael Benedettelli
 */
@Service
public class DateTimeHelper {

    private static String dateMaskPattern;

    private static String zoneISO;

    private static Locale locale;

    public static String getDateNowToPrint() {

        LocalDate localDate = LocalDate.now();

        String dateStr = localDate.format(getDateTimeFormat());

        return dateStr;

    }

    public static String getDateToPrint(LocalDate localDate) {

        String dateStr = localDate.format(getDateTimeFormat());

        return dateStr;

    }

    public static String getDateTimeNowForZone() {

        ZoneId zoneId = (zoneISO != null) ? ZoneId.of(zoneISO) : ZoneId.systemDefault();

        ZonedDateTime zonedDateTime = ZonedDateTime.of(LocalDateTime.now(), zoneId);

        String dateStr = zonedDateTime.format(getDateTimeFormat());

        return dateStr;

    }

    public static LocalDate getDateFromString(String date) {

        LocalDate localDate = LocalDate.parse(date);

        return localDate;

    }

    public static LocalDateTime getDateTimeFromString(String date) {

        LocalDateTime localDateTime = LocalDateTime.parse(date);

        return localDateTime;

    }

    public static long getPeriodDaysFromNowTo(LocalDate endDate) {

        return getPeriodDaysBetweenDates(LocalDate.now(), endDate);
    }

    public static long getPeriodMonthFromNowTo(LocalDate endDate) {

        return getPeriodMonthsBetweenDates(LocalDate.now(), endDate);
    }

    public static long getPeriodYearsFromNowTo(LocalDate endDate) {

        return getPeriodYearsBetweenDates(LocalDate.now(), endDate);
    }

    public static long getPeriodDaysBetweenDates(LocalDate initialDate, LocalDate endDate) {

        return ChronoUnit.DAYS.between(endDate, initialDate);
    }

    public static long getPeriodMonthsBetweenDates(LocalDate initialDate, LocalDate endDate) {

        return ChronoUnit.MONTHS.between(endDate, initialDate);
    }

    public static long getPeriodYearsBetweenDates(LocalDate initialDate, LocalDate endDate) {

        return ChronoUnit.YEARS.between(endDate, initialDate);
    }

    public static String getDateMaskPattern() {

        return dateMaskPattern;

    }

    public static void setDateMaskPattern(String dateMaskPattern) {

        DateTimeHelper.dateMaskPattern = dateMaskPattern;

    }

    public static DateTimeFormatter getDateTimeFormat() {

        DateTimeFormatter format = (dateMaskPattern != null)
                ? DateTimeFormatter.ofPattern(dateMaskPattern).withLocale(Locale.ITALY)
                : DateTimeFormatter.ISO_DATE.withLocale(locale);

        return format;
    }

    public static DateFormat getOldDateFormat() {

        DateFormat format = new SimpleDateFormat(dateMaskPattern, locale);

        return format;
    }

}
