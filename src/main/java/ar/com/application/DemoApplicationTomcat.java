/*
 * Copyright 2019 Veronika.com.ar.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ar.com.application;

import static ar.com.application.ApplicationService.I18N;
import static ar.com.application.ApplicationService.LOGGER;
//import de.codecentric.boot.admin.server.config.EnableAdminServer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * The {@code ApplicationService} concrete class is the secondary alternative
 * entry-point execution project. This concrete class has all logic for startup
 * the application executes according the Spring Boot modal.
 * <p>
 * This class is disabled for execution. If you want to active for alternative
 * execution environment you need remove the comments of
 * {@code @SpringBootApplication}
 *
 * @author Rafael Benedettelli
 * @see ar.com.application#ApplicationService()
 * @see ar.com.application#DatabaseSource
 * @see ar.com.application#DatabaseSourceActive
 * @since 1.0
 */
//@SpringBootApplication
//@EnableAdminServer
public class DemoApplicationTomcat implements ApplicationRunner {      

    @Autowired
    ApplicationService applicationService;

    /**
     * The main method entry-point application.
     *
     * @param args The String arguments
     */
    public static void main(String[] args) {

        SpringApplication.run(DemoApplicationTomcat.class, args);

    }

    @Override
    public void run(ApplicationArguments args) throws Exception {

        applicationService.init();

        LOGGER.info(I18N.getString("app.start.message"));

    }

}
