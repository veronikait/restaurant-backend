/*
 * Copyright 2019 Veronika.com.ar.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ar.com.application.exceptions;

import org.hibernate.HibernateException;

/**
 * The {@code DataAccessLayerException} class represents a Exception Object.
 * This exception is throw when some problems happens at connection time with
 * some kind of storage defined for uses in the application such Databases or
 * Document managements.
 *
 * @author Rafael Benedettelli
 * @see ar.com.application.dao#Dao
 * @see ar.com.application.dao#BaseDao
 * @since 1.0
 */
public class DataAccessLayerException extends Exception {

    /**
     * Constructs a new exception with the specified detail message. The cause
     * is not initialized, and may subsequently be initialized by a call to
     * {@link java.lang.Exception#initCause}.
     *
     * @param e hibernateException. The object for know in detail the real
     * exception cause.
     */
    public DataAccessLayerException(HibernateException e) {

        super("DataAccessLayerException: " + e.getMessage());
    }

}
