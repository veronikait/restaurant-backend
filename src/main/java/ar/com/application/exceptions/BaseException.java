/*
 * Copyright 2019 Veronika.com.ar.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ar.com.application.exceptions;

/**
 * The {@code BaseException} class represents a base Exception Object. This kind
 * of object is a general Exception and uses for abnormal execution proposes.
 * <p>
 * The class {@code BaseException} and its subclasses are a form of
 * {@code Throwable} that indicates conditions that a reasonable
 * application might want to catch.
 *
 * <p>The class {@code BaseException} and any subclasses that are not also
 * subclasses of {@link RuntimeException} are <em>checked
 * exceptions</em>.  Checked exceptions need to be declared in a
 * method or constructor's {@code throws} clause if they can be thrown
 * by the execution of the method or constructor and propagate outside
 * the method or constructor boundary.
 *
 * @author Rafael Benedettelli
 * @see ar.com.application.dao#Dao
 * @see ar.com.application.dao#BaseDao
 * @since 1.0
 */
public class BaseException extends Exception {

    /**
     * Constructs a new exception with the specified detail message. The cause
     * is not initialized, and may subsequently be initialized by a call to
     * {@link java.lang.Exception#initCause}.
     *
     * @param message the detail message. The detail message is saved for later
     * retrieval by the {@link java.lang.Exception#getMessage()} method.
     */
    public BaseException(String message) {

        super(message);
    }

}
