/*
 * Copyright 2019 Veronika.com.ar.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ar.com.application;

import static ar.com.application.ApplicationService.I18N;
import static ar.com.application.ApplicationService.LOGGER;
import ar.com.application.helpers.CommandLineHelper;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
//import de.codecentric.boot.admin.server.config.EnableAdminServer;


/**
 * The {@code ApplicationService} concrete class is the predefined entry-point
 * execution project. This concrete class has all logic for startup the
 * application executes according the Spring Boot modal.
 * <p>
 * This class is critical and is the first entry-point execution for
 * application.
 *
 * @author Rafael Benedettelli
 * @see ar.com.application#ApplicationService()
 * @see ar.com.application#DatabaseSource
 * @see ar.com.application#DatabaseSourceActive
 * @since 1.0
 */
@SpringBootApplication
//@EnableAdminServer
//@ComponentScan(basePackages = "ar.com.application")
public class DemoApplication implements ApplicationRunner {


    @Autowired
    ApplicationService applicationService;

    public static void main(String[] args) {

        SpringApplication.run(DemoApplication.class, args);

    }

    @Override
    public void run(ApplicationArguments args) throws Exception {

        applicationService.init();

        ServerSource server = applicationService.getServerInformation();

        String url = server.getUrlHost();
        url += applicationService.getServletContextPath();
        url += "/swagger-ui.html";

        LOGGER.info(I18N.getString("app.start.message"));

        LOGGER.info("The restaurant app listen in " + url);

        CommandLineHelper cmd = new CommandLineHelper();
        cmd.openUrl(url);
        
        

    }

}
