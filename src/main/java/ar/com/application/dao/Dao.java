/*
 * Copyright 2019 Veronika.com.ar.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ar.com.application.dao;

import java.util.List;
import java.util.Optional;

/**
 * The {@code Dao} interface defined general methods for persistence actions.
 * <p>
 * This interface in conjunction with BaseDao propose a common why to implement
 * all concrete DAO classes. Example of use is UserDAO extends BaseDao and
 * implements this interface DAO. In this way, the UserDao object its obligate
 * to implements all basic DAO Pattern methods but we the plus inheritance of
 * BaseDao the object can delegate the execution to leverage basic mechanism and
 * improve with extra logic.
 *
 *
 * @author Rafael Benedettelli
 * @param <T> The type off DAO class (Sample UserDao).
 * @see ar.com.application.dao#Dao
 * @since 1.0
 */
public interface Dao<T> {

    Optional<T> get(Long id) throws Exception;

    List<T> getAll() throws Exception;

    T save(T t) throws Exception;

    void update(T t, String[] params) throws Exception;

    void delete(T t) throws Exception;

}
