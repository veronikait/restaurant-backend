/*
 * Copyright 2019 Veronika.com.ar.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ar.com.application.dao;

import ar.com.application.ApplicationService;
import ar.com.application.exceptions.DataAccessLayerException;
import java.util.List;
import java.util.Optional;
import java.util.function.Consumer;
import javax.persistence.EntityManager;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 * The {@code BaseDao} class defined general methods for persistence actions.
 * This class has all required code for interact with any kind of relational
 * database storage according the JPA (Hibernate) specifications.
 * <p>
 * All classes that extends this class will make use of this method. All methods
 * are parametrized in generic way using Object class for parameters and
 * Optional Object class for returns.
 *
 * @author Rafael Benedettelli
 * @see ar.com.application.dao#Dao
 * @since 1.0
 */
@Repository
public class BaseDao {

    protected SessionFactory sessionFactory;

    protected Session entityManager;

    private Transaction tx;

    @Autowired
    protected ApplicationService applicationService;

    protected void saveOrUpdate(Object obj) throws DataAccessLayerException {

        executeInsideTransaction(em -> this.entityManager.save(obj));

    }

    protected void purge(Object obj) throws DataAccessLayerException {

        executeInsideTransaction(em -> this.entityManager.persist(obj));
    }

    protected Optional<Object> find(Class clazz, Long id) throws DataAccessLayerException {

        startOperation();

        Object object = null;

        try {

            object = entityManager.find(clazz, id);

            tx.commit();

        } catch (HibernateException e) {

            handleException(e);

        } finally {
            if (entityManager != null) {
                entityManager.close();
            }
        }
        return Optional.ofNullable(object);
    }

    protected List findAll(Class clazz) throws DataAccessLayerException {

        Query query = entityManager.createQuery("from " + clazz.getName());
        return query.getResultList();
    }

    protected void handleException(HibernateException e) throws DataAccessLayerException {

        tx.rollback();
        throw new DataAccessLayerException(e);
    }

    protected void startOperation() throws HibernateException {

        if (sessionFactory == null) {
            
            sessionFactory = applicationService.getSessionFactory();
        
        }

        entityManager = sessionFactory.openSession();
        tx = entityManager.beginTransaction();
    }

    private void executeInsideTransaction(Consumer<EntityManager> action) throws DataAccessLayerException {

        startOperation();

        try {
            action.accept(entityManager);
            tx.commit();
        } catch (HibernateException e) {
            handleException(e);
        } finally {
            if (entityManager != null) {
                entityManager.close();
            }
        }
    }

}
