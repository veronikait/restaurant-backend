/*
 * Copyright 2019 Veronika.com.ar.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ar.com.application.dao;

import static ar.com.application.ApplicationService.LOGGER;
import ar.com.application.domain.Group;
import ar.com.application.domain.User;
import ar.com.application.exceptions.DataAccessLayerException;
import java.util.List;
import java.util.Optional;
import org.springframework.stereotype.Repository;

/**
 * The {@code GroupDao} class implements all method for persistence of Group
 * object model. 
 * 
 * @author Rafael Benedettelli
 * @see ar.com.dao#BaseDao
 * @see ar.com.dao#Dao
 * @see ar.com.dao#UserDao
 * @see ar.com.dao#Group
 * @since 1.0
 */
@Repository
public class GroupDao extends BaseDao implements Dao<Group> {

    @Override
    public Optional<Group> get(Long id) throws DataAccessLayerException {

        LOGGER.info("get object by id " + id);

        return Optional.ofNullable((Group) super.find(Group.class, id).get());

    }

    @Override
    public List<Group> getAll() throws DataAccessLayerException {

        return super.findAll(User.class);

    }

    @Override
    public Group save(Group t) throws DataAccessLayerException {

        super.saveOrUpdate(t);

        return t;
    }

    @Override
    public void update(Group t, String[] params) throws DataAccessLayerException {

        super.saveOrUpdate(t);
    }

    @Override
    public void delete(Group t) throws DataAccessLayerException {

        super.purge(t);
    }

}
