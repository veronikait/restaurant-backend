/*
 * Copyright 2019 Veronika.com.ar.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ar.com.application.services.notifications;

import static ar.com.application.ApplicationService.LOGGER;
import org.apache.commons.mail.Email;
import org.apache.commons.mail.EmailException;
import org.apache.commons.mail.SimpleEmail;
import org.springframework.stereotype.Service;

/**
 *
 * @author Rafael Benedettelli
 */
@Service
public class NotificationSimpleTextMailService extends NotificationBaseService implements NotifiactionService {

    @Override
    public Boolean notificate() {

        try {

            Email email = build(new SimpleEmail());                                    

            email.send();

        } catch (EmailException e) {

            LOGGER.error("error at try to send simple mail " + e);
            return false;
        }

        return true;
    }

}
