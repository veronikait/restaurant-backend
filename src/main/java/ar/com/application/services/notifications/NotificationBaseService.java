/*
 * Copyright 2019 Veronika.com.ar.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ar.com.application.services.notifications;

import ar.com.application.ApplicationService;
import ar.com.application.NotificationSource;
import ar.com.application.domain.Group;
import ar.com.application.domain.User;
import ar.com.application.services.BaseService;
import java.util.ArrayList;
import java.util.List;
import org.apache.commons.mail.DefaultAuthenticator;
import org.apache.commons.mail.Email;
import org.apache.commons.mail.EmailException;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author Rafael Benedettelli
 */
public class NotificationBaseService extends BaseService {

    protected String title;

    protected String subject;

    protected String from;

    protected String sign;

    protected String message;

    protected List<User> users;

    protected List<Group> groups;

    protected List<String> mails;

    protected List<User> usersCc;

    protected List<Group> groupsCc;

    protected List<String> mailsCc;

    protected List<User> usersBcc;

    protected List<Group> groupsBcc;

    protected List<String> mailsBcc;

    protected NotificationSource notificationSource;

    protected final int TIMEOUT_MILLISEC = 1000;

    @Autowired
    protected ApplicationService serviceInitilizeApp;

    protected Email build(Email email) throws EmailException {

        notificationSource = serviceInitilizeApp.getNotifiactionSource();

        email.setHostName(notificationSource.getHost());
        email.setSmtpPort(notificationSource.getPort());
        email.setSSLOnConnect(notificationSource.getSsl());
        email.setAuthenticator(new DefaultAuthenticator(
                notificationSource.getUsername(),
                notificationSource.getPassword()));

        email.setFrom(notificationSource.getFrom());
        email.setSubject(notificationSource.getSubject());
        email.setMsg(notificationSource.getMessage()
                + "\n\n\n\n"
                + notificationSource.getSign());

        email.setSocketConnectionTimeout(TIMEOUT_MILLISEC);
        email.setSocketTimeout(TIMEOUT_MILLISEC);

        //Adding destinataries.................................
        String from = notificationSource.getFrom();

        if (from != null) {

            if (from.contains(",")) {

                String[] mails = from.split(",");

                for (String mail : mails) {
                    email.addTo(mail);
                }

            } else {

                email.addTo(from);
            }

        }

        String cc = notificationSource.getCc();

        if (cc != null) {

            if (cc.contains(",")) {

                String[] mails = cc.split(",");

                for (String mail : mails) {
                    email.addCc(mail);
                }

            } else {

                email.addCc(cc);
            }

        }

        String bcc = notificationSource.getBcc();

        if (bcc != null) {

            if (bcc.contains(",")) {

                String[] mails = bcc.split(",");

                for (String mail : mails) {
                    email.addBcc(mail);
                }

            } else {

                email.addBcc(cc);
            }

        }

        if (users != null) {
            for (User user : users) {
                email.addTo(user.getUsername());
            }
        }

        if (groups != null) {
            for (Group group : groups) {
                for (User user : users) {
                    email.addTo(user.getUsername());
                }
            }
        }

        if (mails != null) {
            for (String mail : mails) {
                email.addTo(mail);
            }
        }

        if (usersCc != null) {
            for (User user : usersCc) {
                email.addCc(user.getUsername());
            }
        }

        if (groupsCc != null) {
            for (Group group : groupsCc) {
                for (User user : users) {
                    email.addCc(user.getUsername());
                }
            }
        }

        if (mailsCc != null) {
            for (String mail : mailsCc) {
                email.addCc(mail);
            }
        }

        if (usersBcc != null) {
            for (User user : usersBcc) {
                email.addBcc(user.getUsername());
            }
        }

        if (groupsBcc != null) {
            for (Group group : groupsBcc) {
                for (User user : users) {
                    email.addBcc(user.getUsername());
                }
            }
        }

        if (mailsBcc != null) {
            for (String mail : mailsBcc) {
                email.addBcc(mail);
            }
        }

        return email;

    }

    public void addUser(User user) {

        if (users == null) {

            users = new ArrayList<>();
        }

        users.add(user);
    }

    public void addGroup(Group group) {

        if (groups == null) {

            groups = new ArrayList<>();
        }

        groups.add(group);
    }

    public void addMail(String mail) {

        if (mails == null) {

            mails = new ArrayList<>();
        }

        mails.add(mail);
    }

    public void addUserCc(User user) {

        if (usersCc != null) {

            usersCc = new ArrayList<>();
        }

        usersCc.add(user);
    }

    public void addGroupCc(Group group) {

        if (groupsCc == null) {

            groupsCc = new ArrayList<>();
        }

        groupsCc.add(group);
    }

    public void addMailCc(String mail) {

        if (mailsCc == null) {

            mailsCc = new ArrayList<>();
        }

        mailsBcc.add(mail);
    }

    public void addUserBcc(User user) {

        if (usersBcc == null) {

            usersBcc = new ArrayList<>();
        }

        usersBcc.add(user);
    }

    public void addGroupBcc(Group group) {

        if (groupsBcc == null) {

            groupsBcc = new ArrayList<>();
        }

        groupsBcc.add(group);
    }

    public void addMailBcc(String mail) {

        if (mailsBcc == null) {

            mailsBcc = new ArrayList<>();
        }

        mailsBcc.add(mail);
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<User> getUsersCc() {
        return usersCc;
    }

    public void setUsersCc(List<User> usersCc) {
        this.usersCc = usersCc;
    }

    public List<Group> getGroupsCc() {
        return groupsCc;
    }

    public void setGroupsCc(List<Group> groupsCc) {
        this.groupsCc = groupsCc;
    }

    public List<String> getMailsCc() {
        return mailsCc;
    }

    public void setMailsCc(List<String> mailsCc) {
        this.mailsCc = mailsCc;
    }

    public List<User> getUsersBcc() {
        return usersBcc;
    }

    public void setUsersBcc(List<User> usersBcc) {
        this.usersBcc = usersBcc;
    }

    public List<Group> getGroupsBcc() {
        return groupsBcc;
    }

    public void setGroupsBcc(List<Group> groupsBcc) {
        this.groupsBcc = groupsBcc;
    }

    public List<String> getMailsBcc() {
        return mailsBcc;
    }

    public void setMailsBcc(List<String> mailsBcc) {
        this.mailsBcc = mailsBcc;
    }

    public NotificationSource getNotificationSource() {
        return notificationSource;
    }

    public void setNotificationSource(NotificationSource notificationSource) {
        this.notificationSource = notificationSource;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getSign() {
        return sign;
    }

    public void setSign(String sign) {
        this.sign = sign;
    }

    public String getContent() {
        return message;
    }

    public void setContent(String content) {
        this.message = content;
    }

    public List<User> getUsers() {
        return users;
    }

    public void setUsers(List<User> users) {
        this.users = users;
    }

    public List<Group> getGroups() {
        return groups;
    }

    public void setGroups(List<Group> groups) {
        this.groups = groups;
    }

    public List<String> getMails() {
        return mails;
    }

    public void setMails(List<String> mails) {
        this.mails = mails;
    }

}
