/*
 * Copyright 2019 Veronika.com.ar.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ar.com.application.services.transformers;

/**
 *
 * @author Rafael Benedettelli
 */
public class TransformerFactoryService {

    public static enum TransformTypes {
        TEXT, HTML, PDF, WORD
    };

    public static TransformService getParserService(TransformTypes type) {

        TransformService transformServices = null;

        switch (type) {

            case PDF:
                transformServices = new TransformPdfService();
                break;

            case HTML:
                transformServices = new TransformHtmlService();
                break;

            case WORD:
                transformServices = new TransformTextService();
                break;

            case TEXT:
                transformServices = new TransformTextService();
                break;

        }

        return transformServices;

    }

}
