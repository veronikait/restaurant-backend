/*
 * Copyright 2019 Veronika.com.ar.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ar.com.application.services.transformers;

import java.nio.file.Path;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Rafael Benedettelli
 */
public class Page {

    private String title;

    private String subTitle;

    private LocalDate date;

    private List<StringBuilder> parragraphs;

    private List<Path> images;

    public Page(String title) {
        this.title = title;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSubTitle() {
        return subTitle;
    }

    public void setSubTitle(String subTitle) {
        this.subTitle = subTitle;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public List<StringBuilder> getParragraphs() {
        return parragraphs;
    }

    public void setParragraphs(List<StringBuilder> parragraph) {
        this.parragraphs = parragraphs;
    }

    public List<Path> getImages() {
        return images;
    }

    public void setImages(List<Path> images) {
        this.images = images;
    }

    public void addImage(Path image) {

        if (images == null) {

            images = new ArrayList<>();

        }

        images.add(image);
    }

    public void addParagraph(StringBuilder parragraph) {

        if (parragraphs == null) {

            parragraphs = new ArrayList<>();

        }

        parragraphs.add(parragraph);
    }

}
