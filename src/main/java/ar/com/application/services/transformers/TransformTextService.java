/*
 * Copyright 2019 Veronika.com.ar.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ar.com.application.services.transformers;

import static ar.com.application.ApplicationService.LOGGER;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Path;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Rafael Benedettelli
 */
public class TransformTextService extends TransformBaseService implements TransformService {

    private static final String BREAKLINE_CHAR = "\n";

    private static final String UNDERLINED_SIMPLE = "________________________________________";

    private static final String UNDERLINED_DOUBLE = "========================================";

    @Override
    public Path transform() throws IOException {

        try (BufferedWriter writer = new BufferedWriter(new FileWriter(path.toAbsolutePath().toString()))) {

            if (author != null) {

                writer.write("@" + author + BREAKLINE_CHAR);
                writer.write(BREAKLINE_CHAR);

            }

            writeTitles(writer, title, subtitle);

            if (pages != null) {

                for (Page page : pages) {

                    writeTitles(writer, page.getTitle(), page.getSubTitle());

                    page.getParragraphs().stream().map((t) -> {

                        return t.toString();

                    }).forEach((str) -> {
                        try {

                            writer.write(str);
                            writer.write(BREAKLINE_CHAR);
                            writer.write(UNDERLINED_DOUBLE);
                            writer.write(BREAKLINE_CHAR);

                        } catch (IOException ex) {

                            LOGGER.info("error at try to write parragraph in text file " + ex);

                        }
                    });

                }

            }

        }

        return path;

    }

    public static void writeTitles(BufferedWriter writer, String title, String subtitle) throws IOException {

        if (title != null) {

            writer.write(title + BREAKLINE_CHAR);
            writer.write(UNDERLINED_SIMPLE);
            writer.write(BREAKLINE_CHAR);

        }

        if (subtitle != null) {

            writer.write(subtitle + BREAKLINE_CHAR);
            writer.write(BREAKLINE_CHAR + BREAKLINE_CHAR);

        }
    }

}
