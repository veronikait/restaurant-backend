/*
 * Copyright 2019 Veronika.com.ar.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ar.com.application.services;

import org.springframework.stereotype.Service;

/**
 * The {@code BaseService} class represents a the base object for all Services
 * classes.
 * <p>
 *
 * BaseService defined all basic logic for prototype some kind of service
 * implementation.
 *
 * @author Rafael Benedettelli
 * @see ar.com.application.dao#Dao
 * @see ar.com.application.dao#BaseDao
 * @since 1.0
 */
@Service
public class BaseService {

    

}
