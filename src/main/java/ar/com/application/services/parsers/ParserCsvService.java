/*
 * Copyright 2019 Veronika.com.ar.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ar.com.application.services.parsers;

import static ar.com.application.services.parsers.ParserServiceJackson.configureMapper;
import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import com.fasterxml.jackson.dataformat.csv.CsvSchema;
import java.io.IOException;
import java.nio.file.Path;
import org.springframework.stereotype.Service;

/**
 *
 * @author Rafael Benedettelli
 */
@Service
public class ParserCsvService extends ParserServiceJackson implements ParserService {

    private static final char SEPARATOR = ',';

    @Override
    public Object parse(Path path, Class clazz) throws IOException {

        CsvMapper csvMapper = new CsvMapper();
        CsvSchema schema = csvMapper
                .typedSchemaFor(clazz)
                .withHeader()
                .withColumnSeparator(SEPARATOR)
                .withComments();

        csvMapper = (CsvMapper) configureMapper(csvMapper);

        return csvMapper
                .readerFor(clazz)
                .with(schema)
                .readValue(path.toFile());

    }

    @Override
    public void serialize(Path path, Object object) throws IOException {

        //Importante!!! los attributos nested (ejemplo: user.place) no se pueden serializar
        //y ademas tira una excepcion! por eso hay que hacer user.setPlace(null); antes de serializar para 
        //cvs
        CsvMapper mapper = new CsvMapper();

        CsvSchema schema = mapper.schemaFor(object.getClass()).withHeader();

        mapper = (CsvMapper) configureMapper(mapper);

        mapper.writer(schema).writeValue(path.toFile(), object);

    }

}
