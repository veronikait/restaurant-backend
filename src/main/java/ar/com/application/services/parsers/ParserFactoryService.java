/*
 * Copyright 2019 Veronika.com.ar.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ar.com.application.services.parsers;

import ar.com.application.helpers.FileHelper;
import java.nio.file.Path;

/**
 *
 * @author Rafael Benedettelli
 */
public class ParserFactoryService {

    public static ParserService getParserService(Path path) {

        String extension = FileHelper.getFileExtension(path);

        extension = extension.toUpperCase();

        ParserService parseService = null;

        switch (extension) {

            case "XML":
                parseService = new ParserXmlService();
                break;

            case "JSON":
                parseService = new ParserJsonService();
                break;

            case "YAML":
                parseService = new ParserYamlService();
                break;

            case "CSV":
            case "XLS":
                parseService = new ParserCsvService();
                break;

        }

        return parseService;

    }

}
