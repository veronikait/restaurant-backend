/*
 * Copyright 2019 Veronika.com.ar.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ar.com.application.services.parsers;

import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import java.nio.file.Path;
import java.util.List;
import org.springframework.stereotype.Service;

/**
 *
 * @author Rafael Benedettelli
 */
@Service
public class ParserJsonService extends ParserServiceJackson implements ParserService {

    @Override
    public Object parse(Path path, Class clazz) throws IOException {

        Object object = configureMapper(new ObjectMapper()).readValue(path.toFile(), clazz);

        return object;
    }

    @Override
    public void serialize(Path path, Object object) throws IOException {

        configureMapper(new ObjectMapper()).writeValue(path.toFile(), object);

    }

   
}
