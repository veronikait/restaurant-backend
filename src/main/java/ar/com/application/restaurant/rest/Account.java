/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ar.com.application.restaurant.rest;

/**
 *
 * @author Rafael Benedettelli
 */
public class Account {

    private String name;

    private String type;

    private String total;

    public Account() {

    }

    public Account(String name, String type, String total) {

        this.name = name;
        this.type = type;
        this.total = total;

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    @Override
    public String toString() {

        return " " + name + " " + type + " " + total;
   
    }

}
