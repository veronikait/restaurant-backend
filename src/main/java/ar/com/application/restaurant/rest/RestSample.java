/*
 * Copyright 2019 Veronika.com.ar.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ar.com.application.restaurant.rest;

import java.util.ArrayList;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Rafael Benedettelli
 */
@RestController
public class RestSample {

    @GetMapping(value = "/getAccounts")
    @ResponseBody
    public ResponseEntity<?> getAccounts() {

        ArrayList<Account> list = new ArrayList<>();
        list.add(new Account("Messi", "cc", "1000"));
        list.add(new Account("Pipita", "ca", "2000"));
        list.add(new Account("Aguero", "cc", "500"));

        return new ResponseEntity(list, HttpStatus.OK);

    }

    @GetMapping(value = "/putAccounts")
    @ResponseBody
    public ResponseEntity<?> putAccount(String name, String type, String total) {

        return new ResponseEntity("OK", HttpStatus.OK);

    }

}
