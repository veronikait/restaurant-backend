/*
 * Copyright 2019 Veronika.com.ar.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ar.com.application.restaurant.rest.docs;

import ar.com.application.ApplicationService;

/**
 *
 * @author Rafael Benedettelli
 */
public class RestDocsConstants {

    private static final String PREFIX = "app.restaurant.rest.";

    public static final String RESTAURANT_ARRIVAL_APIOPERATION_VALUE = ApplicationService.I18N.getString(PREFIX + "verb_arrival_apioperation_value");
    public static final String RESTAURANT_ARRIVAL_APIOPERATION_NOTES = ApplicationService.I18N.getString(PREFIX + "verb_arrival_apioperation_notes");
    public static final String RESTAURANT_ARRIVAL_APIPARAM_VALUE = ApplicationService.I18N.getString(PREFIX + "verb_arrival_apiparam_value");
    public static final String RESTAURANT_ARRIVAL_APIPARAM_EXAMPLE = ApplicationService.I18N.getString(PREFIX + "verb_arrival_apiparame_example");

    public static final String RESTAURANT_LEAVE_APIOPERATION_VALUE = ApplicationService.I18N.getString(PREFIX + "verb_leave_apioperation_value");
    public static final String RESTAURANT_LEAVE_APIOPERATION_NOTES = ApplicationService.I18N.getString(PREFIX + "verb_leave_apioperation_notes");
    public static final String RESTAURANT_LEAVE_APIPARAM_VALUE = ApplicationService.I18N.getString(PREFIX + "verb_leave_apiparam_value");
    public static final String RESTAURANT_LEAVE_APIPARAM_EXAMPLE = ApplicationService.I18N.getString(PREFIX + "verb_leave_apiparame_example");

    public static final String RESTAURANT_WAITLIST_APIOPERATION_VALUE = ApplicationService.I18N.getString(PREFIX + "verb_waitlist_apioperation_value");
    public static final String RESTAURANT_WAITLIST_APIOPERATION_NOTES = ApplicationService.I18N.getString(PREFIX + "verb_waitlist_apioperation_notes");

    public static final String RESTAURANT_STATUS_APIOPERATION_VALUE = ApplicationService.I18N.getString(PREFIX + "verb_status_apioperation_value");
    public static final String RESTAURANT_STATUS_APIOPERATION_NOTES = ApplicationService.I18N.getString(PREFIX + "verb_status_apioperation_notes");

    /*GLOBALS*/
    public static final String RESTAURANT_GLOBAL_APIPARAM_FORMAT_NUMERIC_INTEGER = ApplicationService.I18N.getString(PREFIX + "global_apiparam_format_numeric_integer");
    public static final String RESTAURANT_GLOBAL_APIPARAM_FORMAT_UUID = ApplicationService.I18N.getString(PREFIX + "global_apiparam_format_uuid");

    public static final String RESTAURANT_GLOBAL_APIPARAM_TYPE_PARAM_PATH = ApplicationService.I18N.getString(PREFIX + "global_apiparam_type_param_path");
    public static final String RESTAURANT_GLOBAL_APIPARAM_TYPE_PARAM_QUERY = ApplicationService.I18N.getString(PREFIX + "global_apiparam_type_param_query");

}
