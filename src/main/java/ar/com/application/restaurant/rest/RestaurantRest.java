/*
 * Copyright 2019 Veronika.com.ar.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ar.com.application.restaurant.rest;

import static ar.com.application.ApplicationService.I18N;
import static ar.com.application.ApplicationService.LOGGER;
import ar.com.application.restaurant.domain.Client;
import ar.com.application.restaurant.domain.Table;
import ar.com.application.restaurant.exceptions.RestaurantException;

import ar.com.application.restaurant.rest.domain.SimpleResult;
import ar.com.application.restaurant.services.RestManagerService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.Authorization;
import java.util.List;
import java.util.Optional;
import java.util.Queue;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Api(value = "Restaurant")
public class RestaurantRest {

    private static final String MESSAGE_SUCCESSFULL = "successful";

    @Autowired
    RestManagerService restManager;

    /**
     *
     * @param clientId
     * @return
     */
    @GetMapping(value = "/restaurant/client")
    @ResponseBody
    @ApiOperation(value = "Get a client by id.",
            response = SimpleResult.class,
            authorizations =  @Authorization(value = "basic"),
            notes = "Use this service for retrive one client by id. The result presents the client table if have and all complementary information.")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "OK"),
        @ApiResponse(code = 404, message = "NOT FUOND"),
        @ApiResponse(code = 400, message = "BAD REQUEST")

    }
    )
    public ResponseEntity<?> getClientById(
            @ApiParam(value = "Indicate the size of the new client group of diners.",
                    allowEmptyValue = false,
                    required = true,
                    example = "3a079ecf-5991-46a3-8452-99a32036095f",
                    format = "Java UIID Value 36 digit",
                    type = "Path param")
            @PathVariable String clientId) {

        SimpleResult result = null;

        Optional<Client> optClient = restManager.getClientById(clientId);

        if (optClient.isPresent()) {

            Client client = optClient.get();

            result = new SimpleResult(false, client.toString(), MESSAGE_SUCCESSFULL);
            result.setObject(client.getClientId());
        }

        optClient.orElseThrow(() -> {
            return new RestaurantException(HttpStatus.NOT_FOUND, I18N.getString("app.restaurant.global_message.action.not_found")
                    + " " + I18N.getString("app.restaurant.client.info.id") + clientId);
        });

        return new ResponseEntity(result, HttpStatus.OK);

    }

    /**
     *
     * @param groupSize
     * @return
     */
    @PostMapping(value = "/restaurant/client/{groupSize}")
    @ResponseBody
    @ApiOperation(value = "Register the arrival of new clients to the restaurant.",
            response = SimpleResult.class,
            authorizations =  @Authorization(value = "basic"),
            notes = "Use this service for register new clients arrivals to the restaurant. When execute this service, the client could take a free table if there available place or join to share table. Also, if there is not placed, the client will wait in waiting list for a turn.")
    @ApiResponses(value = {
        @ApiResponse(code = 201, message = "CREATED"),
        @ApiResponse(code = 400, message = "BAD REQUEST")
    }
    )
    public ResponseEntity<?> onArrive(
            @ApiParam(value = "Indicate the size of the new client group of diners.",
                    allowEmptyValue = false,
                    required = true,
                    allowableValues = "1,2,3,4,5,6",
                    example = "1 for single dinner or 6 for a group",
                    format = "Numeric Integer values",
                    type = "Path param")
            @PathVariable Integer groupSize) {

        SimpleResult result;

        try {

            Client client = Client.getInstance(groupSize);

            String eventResult = restManager.onArrive(client);

            result = new SimpleResult(false, eventResult, MESSAGE_SUCCESSFULL);

            result.setObject(client.getClientId());

        } catch (RestaurantException e) {

            LOGGER.error(e);

            throw e;

        }

        return new ResponseEntity(result, HttpStatus.CREATED);

    }

    @DeleteMapping(value = "/restaurant/client/{clientId}")
    @ResponseBody
    @ApiOperation(value = "Register the client's exit of the restaurant.",
            response = SimpleResult.class,
            authorizations =  @Authorization(value = "basic"),
            notes = "When execute this service, the client indicated by parameter clientId will leave the table or the waiting list if was waiting for a turn.")

    @ApiResponses(value = {
        @ApiResponse(code = 202, message = "ACCEPTED"),
        @ApiResponse(code = 400, message = "BAD REQUEST"),
        @ApiResponse(code = 404, message = "NOT FOUND")
    }
    )
    public ResponseEntity<?> onLeave(
            @ApiParam(value = "Indicate the the id of the client for leave the restaurant.",
                    allowEmptyValue = false,
                    required = true,
                    example = "046b6c7f-0b8a-43b9-b35d-6489e6daee91",
                    format = "String Java UUID generator id",
                    type = "Path param")
            @PathVariable String clientId) {

        SimpleResult result = null;

        Optional<Client> optClient = restManager.getClientById(clientId);

        if (optClient.isPresent()) {

            Client client = optClient.get();

            restManager.onLeave(client);

            result = new SimpleResult(false, I18N.getString("app.restaurant.client.event.leaving") + " (" + client.toString() + " ) ", MESSAGE_SUCCESSFULL);
            result.setObject(client.getClientId());
        }

        optClient.orElseThrow(() -> {
            return new RestaurantException(HttpStatus.NOT_FOUND, I18N.getString("app.restaurant.global_message.action.not_found")
                    + " " + I18N.getString("app.restaurant.client.info.id") + clientId);
        });

        return new ResponseEntity(result, HttpStatus.ACCEPTED);

    }

    @GetMapping(value = "/restaurant/waitingList")
    @ResponseBody
    @ApiOperation(value = "Retrive the complete waiting list of restaurant in priority order.",
            response = SimpleResult.class,
            authorizations =  @Authorization(value = "basic"),
            notes = "The service return a json with al clients waiting in queue ordering by priority")

    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "OK"),})
    public ResponseEntity<?> getWaitingList() {

        SimpleResult result;

        try {

            Queue<Client> clientsQueue = restManager.getWaitingList();

            result = new SimpleResult(false, "", MESSAGE_SUCCESSFULL);
            result.setObject(clientsQueue);

        } catch (Exception e) {

            LOGGER.error(e);
            result = new SimpleResult(e.getMessage());
        }

        return new ResponseEntity(result, HttpStatus.OK);

    }

    @GetMapping(value = "/restaurant/status")
    @ResponseBody
    @ApiOperation(value = "Retrive the complete restaurant scenario with all tables.",
            response = SimpleResult.class,
            authorizations =  @Authorization(value = "basic"),
            notes = "The service return a json with all tables and its status (free, busy) and client quantity")

    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "OK"),})
    public ResponseEntity<?> getStatusTables() {

        SimpleResult result;

        try {

            List<Table> restaurantTables = restManager.getRestaurantStateTables();

            result = new SimpleResult(false, "", MESSAGE_SUCCESSFULL);
            result.setObject(restaurantTables);

        } catch (Exception e) {

            LOGGER.error(e);
            result = new SimpleResult(e.getMessage());
        }

        return new ResponseEntity(result, HttpStatus.OK);

    }

}
