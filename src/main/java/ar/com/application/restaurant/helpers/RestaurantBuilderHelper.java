package ar.com.application.restaurant.helpers;

import static ar.com.application.ApplicationService.I18N;
import static ar.com.application.ApplicationService.LOGGER;
import ar.com.application.restaurant.domain.Table;
import ar.com.application.restaurant.exceptions.RestaurantConfigurationException;
import ar.com.application.restaurant.exceptions.RestaurantException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import org.springframework.http.HttpStatus;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Rafael Benedettelli
 */
public class RestaurantBuilderHelper {

    private static List<Table> tables;

    public static int totalTables;

    public static int minChairPerTable;

    public static int maxChairsPerTable;

    private static final Random RANDOM = new Random();

    private static final int MIN_CHAIR_PER_TABLE_ALLOWED = 2;

    public static List<Table> build() throws RestaurantException {

        if (minChairPerTable < MIN_CHAIR_PER_TABLE_ALLOWED) {

            throw new RestaurantConfigurationException(HttpStatus.BAD_REQUEST, I18N.getString("app.restaurant.excpetion.table.size.min_chair_per_table") + MIN_CHAIR_PER_TABLE_ALLOWED);

        }

        if (maxChairsPerTable < minChairPerTable) {

            throw new RestaurantConfigurationException(HttpStatus.BAD_REQUEST, I18N.getString("app.restaurant.excpetion.table.size.max_chair_per_table"));

        }

        tables = new ArrayList<>();

        for (int i = 0; i < totalTables; i++) {

            tables.add(new Table(i, getRandomNumberOfChairs()));
        }

        return tables;
    }

    private static int getRandomNumberOfChairs() {

        int range = maxChairsPerTable - minChairPerTable;

        int chairs = RANDOM.nextInt(range + 1);

        chairs += minChairPerTable;

        return chairs;

    }

    public static void show() {

        LOGGER.info("\n\n___________("+I18N.getString("app.restaurant.configuration.console_graphic.busy_chair_title")+"/"+I18N.getString("app.restaurant.configuration.console_graphic.total_chair_title")+")__\n");
        tables.stream().forEach(
                LOGGER::info
        );

        LOGGER.info("____________________________________\n\n");

    }

}
