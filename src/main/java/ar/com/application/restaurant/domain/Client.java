package ar.com.application.restaurant.domain;

import static ar.com.application.ApplicationService.I18N;
import ar.com.application.restaurant.exceptions.RestaurantConfigurationException;
import ar.com.application.restaurant.exceptions.RestaurantException;
import com.fasterxml.jackson.annotation.JsonIgnore;
import java.util.UUID;
import org.springframework.http.HttpStatus;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Rafael Benedettelli
 */
public class Client {

    @JsonIgnore
    protected Table table;

    protected String clientId;

    private static int minClientArrive;

    private static int maxClientArrive;

    private static final int MIN_CLIENT_ALLOWED = 1;

    public static Client getInstance(int size) throws RestaurantException {

        if (minClientArrive < MIN_CLIENT_ALLOWED) {

            throw new RestaurantConfigurationException(HttpStatus.BAD_REQUEST, I18N.getString("app.restaurant.configuration.excpetion.client.size.min_invalid") + MIN_CLIENT_ALLOWED);

        }

        if (maxClientArrive < minClientArrive) {

            throw new RestaurantConfigurationException(HttpStatus.BAD_REQUEST, I18N.getString("app.restaurant.configuration.excpetion.client.size.max_invalid"));

        }

        Client client = null;

        if (size >= minClientArrive && size <= maxClientArrive) {

            if (size == MIN_CLIENT_ALLOWED) {

                client = new Client("");

            } else {

                client = new ClientGroup("", size);

            }
        } else {

            throw new RestaurantException(HttpStatus.BAD_REQUEST, I18N.getString("app.restaurant.excpetion.client.size.out_of_range") + "(" + minClientArrive + " - " + maxClientArrive + ")");
        }

        client.clientId = UUID.randomUUID().toString();

        return client;

    }

    public Client(String enlistName) {
        this.clientId = enlistName;
    }

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String enlistName) {
        this.clientId = enlistName;
    }

    public Table getTable() {
        return table;
    }

    public void setTable(Table table) {

        table.addClient(this);
        this.table = table;
    }

    public int getSize() {

        int size = 0;

        if (this != null) {

            size = (this instanceof ClientGroup) ? ((ClientGroup) this).getSize() : 1;

        }

        return size;
    }

    public boolean hasTable() {

        return this.getTable() != null;

    }

    public void leave() {

        if (this.hasTable()) {

            table.leaveClient(this);

        }

    }

    public static int getMinClientArrive() {
        return minClientArrive;
    }

    public static void setMinClientArrive(int minClientArrive) {
        Client.minClientArrive = minClientArrive;
    }

    public static int getMaxClientArrive() {
        return maxClientArrive;
    }

    public static void setMaxClientArrive(int maxClientArrive) {
        Client.maxClientArrive = maxClientArrive;
    }

    @Override
    public String toString() {

        String clientStr = " " + I18N.getString("app.restaurant.client.info.id") + clientId;

        clientStr += " " + I18N.getString("app.restaurant.client.info.size") + getSize();

        if (table != null) {
            clientStr += " " + I18N.getString("app.restaurant.client.info.table") + table.getTableNumber();
        }

        return clientStr;

    }

}
