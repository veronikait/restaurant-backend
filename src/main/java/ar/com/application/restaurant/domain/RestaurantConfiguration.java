/*
 * Copyright 2019 Veronika.com.ar.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ar.com.application.restaurant.domain;

/**
 *
 * @author Rafael Benedettelli
 */
public class RestaurantConfiguration {

    private Integer totalTables;

    private Integer minChairsPerTable;

    private Integer maxChairsPerTable;

    private Integer minClientArriving;

    private Integer maxClientArriving;

    private Boolean showConsoleGraphic;

    public Integer getTotalTables() {
        return totalTables;
    }

    public void setTotalTables(Integer totalTables) {
        this.totalTables = totalTables;
    }

    public Integer getMinChairsPerTable() {
        return minChairsPerTable;
    }

    public void setMinChairsPerTable(Integer minChairsPerTable) {
        this.minChairsPerTable = minChairsPerTable;
    }

    public Integer getMaxChairsPerTable() {
        return maxChairsPerTable;
    }

    public void setMaxChairsPerTable(Integer maxChairsPerTable) {
        this.maxChairsPerTable = maxChairsPerTable;
    }

    public Integer getMinClientArriving() {
        return minClientArriving;
    }

    public void setMinClientArriving(Integer minClientArriving) {
        this.minClientArriving = minClientArriving;
    }

    public Integer getMaxClientArriving() {
        return maxClientArriving;
    }

    public void setMaxClientArriving(Integer maxClientArriving) {
        this.maxClientArriving = maxClientArriving;
    }

    public Boolean isShowConsoleGraphic() {
        return showConsoleGraphic;
    }

    public void setShowConsoleGraphic(Boolean showConsoleGraphic) {
        this.showConsoleGraphic = showConsoleGraphic;
    }

}
