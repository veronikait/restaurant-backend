package ar.com.application.restaurant.domain;

public class ClientGroup extends Client {

    private int size;

    public ClientGroup(String enlistName, int size) {

        super(enlistName);

        this.size = size;

    }

    @Override
    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

}
