package ar.com.application.restaurant.domain.comparators;

import ar.com.application.restaurant.domain.Table;
import java.util.Comparator;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Rafael Benedettelli
 */
public class TablesAccurateComparator implements Comparator<Table> {

    private int sizeClient;

    @Override
    public int compare(Table t, Table t1) {

        int differseForT = t.getFreeChairs() - sizeClient;
        int differseForT1 = t1.getFreeChairs() - sizeClient;

        return differseForT - differseForT1;

    }

    public int getSizeClient() {
        return sizeClient;
    }

    public void setSizeClient(int sizeClient) {
        this.sizeClient = sizeClient;
    }

}
