package ar.com.application.restaurant.domain;

import static ar.com.application.ApplicationService.I18N;
import java.util.HashSet;
import java.util.Set;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
public class Table {

    private int tableNumber;

    private int chairs;

    private Set<Client> clients;

    private static boolean showGraphic;

    private int countGraphicChair;

    public Table(int tableNumber, int chairs) {
        this.tableNumber = tableNumber;
        this.chairs = chairs;
    }

    public boolean isFree() {

        return clients == null || clients.isEmpty();

    }

    public int getFreeChairs() {

        return chairs - getBusyChairs();

    }

    public int getBusyChairs() {

        int total = 0;

        if (clients != null) {

            for (Client client : clients) {

                total += client.getSize();

            }
        }

        return total;

    }

    public int getTableNumber() {
        return tableNumber;
    }

    public void setTableNumber(int tableNumber) {
        this.tableNumber = tableNumber;
    }

    public int getChairs() {
        return chairs;
    }

    public void setChairs(int chairs) {
        this.chairs = chairs;
    }

    public Set<Client> getClients() {
        return clients;
    }

    public void addClient(Client client) {

        if (clients == null) {
            clients = new HashSet<>();
        }

        clients.add(client);

    }

    public void leaveClient(Client client) {

        if (clients != null) {

            clients.remove(client);

        }

    }

    public static boolean isShowGraphic() {
        return showGraphic;
    }

    public static void setShowGraphic(boolean showGraphic) {
        Table.showGraphic = showGraphic;
    }

    private String chairToString(boolean frontSide) {

        StringBuilder strBuilder = new StringBuilder();

        if (frontSide) {

            countGraphicChair = 0;
        }

        final String bussyChair = I18N.getString("app.restaurant.configuration.console_graphic.busy_chair");

        final String freeChair = I18N.getString("app.restaurant.configuration.console_graphic.free_chair");;

        final String space = " ";

        int totalChairs = getChairs();
        int busyChairs = getBusyChairs();

        int chairsBySide = totalChairs / 2;

        if (!frontSide) {
            chairsBySide = totalChairs;
        }

        for (; countGraphicChair < chairsBySide; countGraphicChair++) {

            if (countGraphicChair < busyChairs) {

                strBuilder.append(space).append(bussyChair);

            } else {

                strBuilder.append(space).append(freeChair);

            }
        }

        return strBuilder.toString();
    }

    @Override
    public String toString() {

        StringBuilder strBuilder = new StringBuilder();

        strBuilder.append(I18N.getString("app.restaurant.table.info.number")).append(tableNumber);

        strBuilder.append(" (").append(getBusyChairs()).
                append("/").append(chairs).append(")");

        strBuilder.append("\n");

        if (showGraphic) {

            strBuilder.append(chairToString(true));

            strBuilder.append("\n");
            strBuilder.append(" _______________ \n");
            strBuilder.append("|               |\n");
            strBuilder.append("|               |\n");
            strBuilder.append("|               |\n");
            strBuilder.append("|_______________|\n");
            strBuilder.append("                 \n");

            strBuilder.append(chairToString(false)).append("\n\n");

        }

        return strBuilder.toString();
    }

}
