/*
 * Copyright 2019 Veronika.com.ar.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ar.com.application.restaurant.exceptions;

import ar.com.application.ApplicationService;
import static ar.com.application.ApplicationService.I18N;
import org.springframework.http.HttpStatus;

/**
 *
 * @author Rafael Benedettelli
 */
public class RestaurantConfigurationException extends RestaurantException {
    
    public RestaurantConfigurationException(HttpStatus httpStatus, String message) {
        
        super(httpStatus, I18N.getString("app.restaurant.configuration.excpetion.message.prefix")
                + message
                + I18N.getString("app.restaurant.configuration.excpetion.message.detail"));
        
    }
    
}
