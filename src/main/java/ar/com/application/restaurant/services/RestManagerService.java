/*
 * Copyright 2019 Veronika.com.ar.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ar.com.application.restaurant.services;

import ar.com.application.restaurant.domain.Client;
import ar.com.application.restaurant.domain.Table;
import java.util.List;
import java.util.Optional;
import java.util.Queue;

/**
 *
 * @author Rafael Benedettelli
 */
public interface RestManagerService {

    public String onArrive(Client client);

    public String onLeave(Client client);

    public Optional<Client> getClientById(String clientId);

    public Queue<Client> getWaitingList();

    public List<Table> getRestaurantStateTables();

    public Optional<Table> lookup(Client client);

}
