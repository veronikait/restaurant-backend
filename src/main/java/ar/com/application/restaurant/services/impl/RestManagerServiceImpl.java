package ar.com.application.restaurant.services.impl;

import ar.com.application.ApplicationService;
import static ar.com.application.ApplicationService.I18N;
import static ar.com.application.ApplicationService.LOGGER;
import ar.com.application.restaurant.domain.Client;
import ar.com.application.restaurant.domain.RestaurantConfiguration;
import ar.com.application.restaurant.domain.Table;
import ar.com.application.restaurant.domain.comparators.TablesAccurateComparator;
import ar.com.application.restaurant.helpers.RestaurantBuilderHelper;
import ar.com.application.restaurant.services.RestManagerService;
import java.util.List;
import java.util.Optional;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class RestManagerServiceImpl implements RestManagerService {

    private List<Table> tables;

    private Queue<Client> waitingListQueue;

    @Autowired
    public RestManagerServiceImpl(ApplicationService applicationService) {

        RestaurantConfiguration initialConf = applicationService.getRestaurantConfiguration();

        RestaurantBuilderHelper.totalTables = initialConf.getTotalTables();
        RestaurantBuilderHelper.minChairPerTable = initialConf.getMinChairsPerTable();
        RestaurantBuilderHelper.maxChairsPerTable = initialConf.getMaxChairsPerTable();

        Table.setShowGraphic(initialConf.isShowConsoleGraphic());
        Client.setMinClientArrive(initialConf.getMinClientArriving());
        Client.setMaxClientArrive(initialConf.getMaxClientArriving());

        tables = RestaurantBuilderHelper.build();

        waitingListQueue = new ConcurrentLinkedQueue<>();

    }

    @Override
    public synchronized String onArrive(Client client) {

        LOGGER.info(I18N.getString("app.restaurant.client.event.arrive") + client);

        waitingListQueue.offer(client);

        showWaitingList();

        String event = processWaitingList();

        RestaurantBuilderHelper.show();

        return event;

    }

    @Override
    public String onLeave(Client client) {

        LOGGER.info(I18N.getString("app.restaurant.client.event.leaving") + client);

        client.leave();

        waitingListQueue.remove(client);

        showWaitingList();

        RestaurantBuilderHelper.show();

        String event = processWaitingList();

        return event;

    }

    @Override
    public Optional<Client> getClientById(String clientId) {

        //Find in tables
        Optional<Optional<Client>> client = tables.stream().filter((t) -> {

            return !t.isFree();

        }).map((t) -> {

            return t.getClients().stream().filter((c) -> {
                return c.getClientId().equalsIgnoreCase(clientId);
            }).findAny();

        }).findAny();

        if (client.isPresent()) {

            return client.get();

        } else {

            //Find in waitingList
            return waitingListQueue.stream().filter((c) -> {

                return c.getClientId().equalsIgnoreCase(clientId);

            }).findAny();
        }

    }

    @Override
    public Optional<Table> lookup(Client group) {

        return tables.stream().filter((table) -> {

            return table.getClients().equals(group);

        }).findAny();

    }

    @Override
    public Queue<Client> getWaitingList() {
        return waitingListQueue;
    }

    @Override
    public List<Table> getRestaurantStateTables() {

        return tables;
    }

    public String processWaitingList() {

        String eventResult = I18N.getString("app.restaurant.client.event.wait");

        if (!waitingListQueue.isEmpty()) {

            //Process in FIFO.
            for (Client client : waitingListQueue) {

                List<Table> avaliableTables = tables.stream().filter((table) -> {

                    return table.getFreeChairs() >= client.getSize() && table.isFree();

                }).collect(Collectors.toList());

                //Scenario where there are free tables.
                if (!avaliableTables.isEmpty()) {

                    avaliableTables.sort(new TablesAccurateComparator());

                    Optional<Table> accurateTable = avaliableTables.stream().findFirst();

                    accurateTable.ifPresent(client::setTable);

                    if (client.hasTable()) {

                        waitingListQueue.remove(client);

                        LOGGER.info(I18N.getString("app.restaurant.client.event.assign") + " " + client.getTable().getTableNumber() + "\n");

                        eventResult = I18N.getString("app.restaurant.client.event.assign") + " " + client.getTable().getTableNumber();

                        //Recursive call while
                        processWaitingList();

                    }

                } else {

                    //Scenario where there are no free tables.                                          
                    List<Table> avaliableShareTables = tables.stream().filter((table) -> {

                        return table.getFreeChairs() >= client.getSize() && !table.isFree();

                    }).collect(Collectors.toList());

                    Optional<Table> accurateTable = avaliableShareTables.stream().findFirst();

                    accurateTable.ifPresent(client::setTable);

                    if (client.hasTable()) {

                        waitingListQueue.remove(client);

                        LOGGER.info(I18N.getString("app.restaurant.client.event.join") + client.getTable().getTableNumber() + "\n");
                        eventResult = I18N.getString("app.restaurant.client.event.join") + client.getTable().getTableNumber();

                        //Recursive call while
                        processWaitingList();

                    }

                }

            }
        }

        return eventResult;
    }

    public List<Table> getTables() {
        return tables;
    }

    public Queue<Client> getWaitingListQueue() {
        return waitingListQueue;
    }

    private void showWaitingList() {

        LOGGER.info(I18N.getString("app.restaurant.waiting_list"));

        waitingListQueue.stream().forEach((c) -> {
            LOGGER.info("[" + c.getSize() + "] ");
        });

        LOGGER.info("\n");

    }

}
